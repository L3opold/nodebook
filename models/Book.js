var mongoose = require('mongoose');

var BookSchema = new mongoose.Schema({
    title: String
});

module.exports = mongoose.model('Book', BookSchema);